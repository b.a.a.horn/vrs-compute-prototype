module VRC.Pipeline where

import VRC.Types
import VRC.Utils
import VRC.Primitives (findTimestep, relevantList)
import VRC.RegionOperators (batchedMap, batchedStencil)
import Data.Array.Accelerate as A

import Debug.Trace

-- Here we describe the core primitives 

-- The core functions in which work is done using

-- | Create a representation of a "Pipeline"
--
-- Using it works like:
-- >>> :type P.ratedSetup (25, 25) nopMotion (xRecalibration 10) ((100, 100), mat)
-- P.ratedSetup (25, 25) nopMotion (xRecalibration 10) ((100, 100), mat) :: Pipeline Int
--
-- Currently lacking an instance of show for our custom function types, so
-- :type has to be used.
ratedSetup :: Elt a
           => RegionDIMS                -- ^ Specify this pipelines region
                                        --  dimensions.
           -> MotionFun a               -- ^ Motion Function
           -> RecalibrationCondition a  -- ^ Recalibration Condition
           -> Acc (Matrix a)            -- ^ Initial data to base the RM off.
           -> Pipeline a
ratedSetup dims motionf rcond initial =
        (dims, ratematrix, motionf, rcond)
    where
        -- Create the inital RMs
        -- So for some reason we wouldn't get get `shape` to work correctly and
        -- not trigger an exception, just passing the dimen
        emptyrm = unlift $ emptyRateMatrix dims initial
        ratematrix = motionf emptyrm initial

-- | Runs ratedStep for `i` steps, returning the end result along with the
-- updated pipeline state.
ratedLoop :: Elt a
          => Pipeline a         -- ^ Pipeline
          -> Timestep           -- ^ Current timestep, decremented each loop
                                -- as this is recursive.
          -> Operation a        -- ^ Operation we are applying
          -> Acc (Matrix a)     -- ^
          -> (Pipeline a, Acc (Matrix a)) -- ^ 
ratedLoop pipeline 0 _ values = (pipeline, values)
ratedLoop pipeline i op values =
        ratedLoop newPipeline (i - 1) newOp newValues
    where
        (newPipeline, newOp, newValues) = ratedStep pipeline i op values

-- | A single time step in the RatedLoop
ratedStep :: Elt a
          => Pipeline a     -- ^ Pipeline 
          -> Timestep       -- ^ Current time step
          -> Operation a    -- ^ Operation to be applied
          -> Acc (Matrix a) -- ^ 
          -> (Pipeline a, Operation a, Acc (Matrix a))
ratedStep (dims, ratem, motionf, rcond) timestep op values =
        ((dims, rm, motionf, rcond), operation, operation timestep values)
    where
        -- decide if we need to update the ratematrix based on new information.
        (rm, operation) =
            if rcond timestep values
               then -- Update the ratematrix, and regenerate the operation.
                (motionf ratem values, op)
               else
                (ratem, op)

-- The next two functions are rated version of the map and stencil primitives.
-- 
-- To use these in a pipeline, you need to 

-- | This applies a map operation that takes into account the rate of the
-- regions.
--
-- You need to give it a RateMatrix, a Timestep, a function and a input. 
--
-- This doesn't require double buffering and can work over the existing dataset.
-- 
-- First we take in a Rate Matrix and a function we want to apply over input.
-- This will then figure out 
ratedMap :: Elt m
         => RateMatrix          -- ^ RateMatrix
         -> RegionDIMS          -- ^ Region Dimensions
         -> (Exp m -> Exp m)    -- ^ Map operation
         -> Timestep            -- ^ Current timestep
         -> Acc (Matrix m)      -- ^ Input Matrix
         -> Acc (Matrix m)      -- ^ Resulting Matrix
ratedMap rm dims f timestep inp =
        writes
    where
        relevant = relevantList $ findTimestep timestep rm
        writes = batchedMap dims relevant f inp

-- op :: Operation a
-- op timestep a = findTimestep timestep

-- | Stencil Function that takes into account rates.
-- With this, we do need to maintain a double buffer as we might need to read
-- values that we eventually modify.
--
-- This requires double buffering.
--
-- 3x3 stencils only currently, but supporting more is just dependent on
-- adjusting the type signature.
ratedStencil :: Elt m
             => RateMatrix                  -- ^ RateMatrix
             -> RegionDIMS                  -- ^ Region Dimensions
             -> (Stencil3x3 m -> Exp m)     -- ^ Stencil operation
             -> Timestep                    -- ^ Current timestep
             -> Acc (Matrix m)              -- ^ Input Matrix
             -> Acc (Matrix m)              -- ^ Resulting Matrix
ratedStencil rm dims f timestep inp =
        writes
    where
        relevant = relevantList $ findTimestep timestep rm
        writes = batchedStencil dims relevant f inp
