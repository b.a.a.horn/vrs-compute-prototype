{-# LANGUAGE TypeOperators #-}

module VRC.Utils where

import VRC.Primitives
import VRC.Types

import Data.Array.Accelerate as A
-- Here we describe the core primitives 

-- | Same RM no mater what you give to it.
nopMotion :: MotionFun a
nopMotion x _ = x

-- | Sets the RM to the same timestep.
-- Useful in emulation of non RM stuff.
allMotion :: Timestep -> MotionFun a
allMotion ts x _ = A.map (\y -> constant ts) x


centerMotion :: MotionFun a
centerMotion x _ =
        x
    where
        Z:.height:.width = unlift $ shape x :: Z:. Exp Int :. Exp Int

-- | Completely impractical motion function for demoing.
increasingMotion :: MotionFun a
increasingMotion x _ = enumFromN (shape x) 1 :: RateMatrix

-- | Always true, so it's recalibrate every step
nopRecalibration :: RecalibrationCondition a
nopRecalibration _ _ = True

-- | Always true, so it's recalibrate every step
noRecalibration :: RecalibrationCondition a
noRecalibration _ _ = False

-- | Recalibrate every X steps
--
-- Fairly naive approach to this, but will work pretty well for most usecases.
xRecalibration :: Timestep -> RecalibrationCondition a
xRecalibration updaterate curr _ = (curr `mod` updaterate) Prelude.== 0

-- | Function to create an initial empty rate matrix
--
-- >>> let m = run $ fill (constant (Z:.10:.10)) 1 :: Matrix Int
-- >>> run $ emptyRateMatrix (5, 5) (use m)
-- Matrix (Z :. 2 :. 2)
--  [ 0, 0,
--    0, 0]
emptyRateMatrix :: Elt a
                => RegionDIMS       -- ^ Region Size
                -> Acc (Matrix a)   -- ^ Matrix size 
                -> RateMatrix       -- ^ End Result
emptyRateMatrix (r_width, r_height) initial =
        fill (lift (Z:.h:.w)) 1
    where
        -- getShape initial
        Z:.y:.x = unlift $ shape initial ::  Z :. Exp Int :. Exp Int
        w = x `div` constant r_width
        h = y `div` constant r_height
