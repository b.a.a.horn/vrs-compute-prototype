module VRC.RegionOperators where

import Debug.Trace
import VRC.Types
import VRC.Primitives
import Data.Array.Accelerate as A
import Data.Array.Accelerate.LLVM.Native as CPU

-- | Apply a operation over a region.
--
-- This a rather inefficient implementation, but as accelerate lacks suitable
-- primitives for this (hence the research), this is how you generally have to
-- do this.
--
-- >>> let mat = fromList (Z:.5:.10) [0..] :: Matrix Int
-- >>> mapRegion (3, 3) (constant 0) (constant 0) (*2) (use mat)
-- Matrix (Z :. 5 :. 10)
--  [  0,  2,  4,  3,  4,  5,  6,  7,  8,  9,
--    20, 22, 24, 13, 14, 15, 16, 17, 18, 19,
--    40, 42, 44, 23, 24, 25, 26, 27, 28, 29,
--    30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
--    40, 41, 42, 43, 44, 45, 46, 47, 48, 49]
-- 
mapRegion :: Elt m
          => RegionDIMS         -- ^ Region of Interests dimensions
          -> Exp Int            -- ^ X offset
          -> Exp Int            -- ^ Y offset
          -> (Exp m -> Exp m)   -- ^ Function to apply over the region.
          -> Acc (Matrix m)     -- ^ Input
          -> Acc (Matrix m)     -- ^ Result
mapRegion dims x y =
    batchedMap dims [(x, y)]

-- | The internal part of mapRegion, where it just returns the data what needs
-- to be written.
mapPartial :: Elt m
           => RegionDIMS         -- ^ Region of Interests dimensions
           -> Exp Int            -- ^ X offset
           -> Exp Int            -- ^ Y offset
           -> (Exp m -> Exp m)   -- ^ Function to apply over the region.
           -> Acc (Matrix m)     -- ^ Input
           -> Acc (Matrix m)     -- ^ Result
mapPartial dims x y f inp =
        A.map f $ readRegion dims x y inp

-- | Batch map a function onto a list of regions onto an input matrix
-- Sequential in this implementation, but it really doesn't need to be.
-- more support / a better understanding of Accelerate is what's needed.
--
-- >>> let mat = fromList (Z:.5:.10) [0..] :: Matrix Int
-- >>> run $ batchWrite (3,3) [(constant 0, constant 0), (constant 1, constant 0)] (*2) (use mat)
-- Matrix (Z :. 5 :. 10)
--  [  0,  2,  4,  6,  8, 10,  6,  7,  8,  9,
--    20, 22, 24, 26, 28, 30, 16, 17, 18, 19,
--    40, 42, 44, 46, 48, 50, 26, 27, 28, 29,
--    30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
--    40, 41, 42, 43, 44, 45, 46, 47, 48, 49]
batchedMap :: Elt m
           => RegionDIMS 
           -> [(Exp Int, Exp Int)]
                                -- ^ Position and data to write there
           -> (Exp m -> Exp m)  -- ^ Function to apply.
           -> Acc (Matrix m)    -- ^ Input
           -> Acc (Matrix m)    -- ^ Result
batchedMap _ [] _ inp = inp 
batchedMap dims@(width, height) ((x, y) : rest) f inp =
        writeRegion dims x' y' region previous
    where
        x' = x * constant width
        y' = y * constant height
        region = mapPartial dims x' y' f inp
        previous = batchedMap dims rest f inp

-- | Applies a Stencil operation over a region.
--
-- As with the map operation, this is also fairly inefficient for the same
-- reason.
stencilRegion :: Elt m
              => RegionDIMS                 -- ^ Region dimensions
              -> Exp Int                    -- ^ X offset
              -> Exp Int                    -- ^ Y offset
              -> (Stencil3x3 m -> Exp m)    -- ^ Stencil operation
              -> Acc (Matrix m)             -- ^ Input Matrix
              -> Acc (Matrix m)             -- ^ Result
stencilRegion dims@(width, height) x y =
    batchedStencil dims [(x, y)]

-- | Applies several stencil operations over the same matrix.
batchedStencil :: Elt m
               => RegionDIMS
               -> [(Exp Int, Exp Int)]
               -> (Stencil3x3 m -> Exp m)
               -> Acc (Matrix m)
               -> Acc (Matrix m)
batchedStencil _ []  _ inp = inp
batchedStencil dims@(width, height) ((x, y) : rest) f inp =
        batchedStencil dims rest f $ 
            writeRegion dims x' y' processedRegion inp
    where
        x' = x * constant width
        y' = y * constant height
        -- We're keeping it to just 3x3, so we just need a single extra pixel
        offset_x = 1
        offset_y = 1
        -- So now we need to copy the data in.
        -- We handle the boundary condition in readRegion.

        inr = readRegion (width + 2*offset_x, height + 2 * offset_y)
            (x' - constant offset_x) (y' - constant offset_y) inp
        -- Apply the stencil. The actual boundary function isn't relevant here,
        -- as we have to deal with that when we copy data into the tmp buffer.
        -- So we'll just use a clamp.
        tmpRegion = A.stencil f clamp inr
        -- Write it back to matrix of the right size.
        processedRegion = readRegion dims
            (constant offset_x) (constant offset_y) tmpRegion
