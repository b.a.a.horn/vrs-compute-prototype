{-# LANGUAGE TypeOperators #-}

module VRC.Primitives where

import VRC.Types
import Data.Array.Accelerate as A
import Data.Array.Accelerate.LLVM.Native as CPU
-- Here we describe the core primitives 

-- | Gets a view into a region, which is a subset of an existing matrix
--
-- Currently this has to copy it into a separate matrix, but ideally this would
-- be able to provide these views through accelerate.
readRegion :: Elt m
           => RegionDIMS        -- ^ Region Dimensions
           -> Exp Int           -- ^ X offset
           -> Exp Int           -- ^ Y offset
           -> Acc (Matrix m)    -- ^ Input Matrix
           -> Acc (Matrix m)    -- ^ Destination
readRegion dims@(width, height) x y inp =
        backpermute sh gatherfun inp
    where
        sh = constant (Z :. height :. width :: DIM2)
        Z:.sHeight:.sWidth = unlift $ shape inp :: Z:. Exp Int :. Exp Int
        gatherfun = idxRead (sWidth, sHeight) dims x y

-- | Returns an Index Permutation function for the read region operation.
idxRead :: (Exp Int, Exp Int)
        -> RegionDIMS   -- ^ Dimensions of the region being read.
        -> Exp Int      -- ^ X offset
        -> Exp Int      -- ^ Y offset
        -> (Exp DIM2 -> Exp DIM2)
                        -- ^ Index function for backpermute
idxRead (width, height) _ offset_x offset_y =
        lift1 f
    where
        -- Implements the boundary for oob reads
        -- we don't expect to overflow with width/height with the ox/oy values,
        -- so this should be fine.
        f :: Z :. Exp Int :. Exp Int -> Z :. Exp Int :. Exp Int
        f (Z:.y:.x) = Z :. y' :. x'
            where
                ox = offset_x + x
                oy = offset_y + y
                y' = cond (oy A.< 0)
                        (height + oy)
                        (oy `mod` height)
                x' = cond (ox A.< 0)
                        (width + ox)
                        (ox `mod` width)



-- | Writes a region back into a matrix, at the specified x/y cords.
writeRegion :: Elt m
            => RegionDIMS       -- ^ Region dimensions
            -> Exp Int          -- ^ X offset
            -> Exp Int          -- ^ Y offset
            -> Acc (Matrix m)   -- ^ Region
            -> Acc (Matrix m)   -- ^ Original Matrix
            -> Acc (Matrix m)   -- ^ Result
writeRegion dims@(width, height) x y region dest =
        permute const dest scatterfun region
    where
        sh = constant (Z :. height :. width :: DIM2)
        scatterfun = idxWrite dims x y

-- | Index Permutation function for the read region operation.
idxWrite :: RegionDIMS  -- ^ Region dimensions
         -> Exp Int     -- ^ X offset
         -> Exp Int     -- ^ Y offset
         -> (Exp DIM2 -> Exp (Maybe DIM2))
                        -- ^ A Index function for Permute.
idxWrite (width, height) offset_x offset_y =
        lift1 f
    where
        f :: Z :. Exp Int :. Exp Int -> Maybe (Z :. Exp Int :. Exp Int)
        f (Z:.y:.x) =
            Just (Z :. offset_y + y :. offset_x + x)

-- | This is an Accelerate Function that reads the Rate matrix and returns all
-- indexes of elements that satisfy our condition.
--
-- Used to determine what areas are relevant at this timestep.
--
-- Example usage:
-- 
-- >>> let mat = fromList (Z:.4:.4) [0..] :: Matrix Float
-- >>> run $ findRelevant (\x -> x A.>= 15 A.&& x A./= 0) (use mat)
-- (Vector (Z :. 1) [(Z :. 3 :. 3,15.0)],Scalar Z [1])

relevantRegions :: (Exp Timestep -> Exp Bool)  -- ^ Decides if relevant.
                -> RateMatrix                  -- ^ Rate Matrix
                -> Acc (Vector (DIM2, Int))
                                            -- ^ Result

relevantRegions cond mat = afst
    $ A.filter (cond . A.snd)
    $ A.flatten
    $ indexed mat

-- | Filters and returns all points active at this timestep.
--
-- Uses relevantRegions with a predefined condition.
findTimestep :: Timestep        -- ^ Current timestep
             -> RateMatrix      -- ^ Rate Matrix
             -> Acc (Vector (DIM2, Int))
                                -- ^ Result
findTimestep timestep =
    relevantRegions (\x -> ((timestep' `mod` x) A.== 0) A.&& x A./= 0)
    where
        timestep' = constant timestep


relevantList :: Acc (Vector (DIM2, Int)) -> [(Exp Int, Exp Int)]
relevantList inp =
        -- Annoyingly we have to run this. Hopefully we can find a way around
        -- that.
        Prelude.map f $ toList $ run inp
    where
        f = \(Z:.y:.x, _) -> (constant x, constant y)
