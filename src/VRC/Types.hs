module VRC.Types where

import Data.Array.Accelerate as A
-- Here we describe the core primitives 

-- | A type alias representing a timestep.
type Timestep = Int

-- | The rate matrix represents regions in our 'VRS' structure for which we
-- sample the rate (i.e, how frequently it should be updated)
type RateMatrix = Acc (Matrix Timestep)


type DIMS = (Int, Int)
-- | Region Dimensions.
type RegionDIMS = DIMS

-- | This is the type for a function that works to recalibrate the RateMatrix
-- based on the contents of the main matrix.
--
-- 'a' is the type of the data we are working on.
--
-- TODO: Define a instance of Show for these
type MotionFun a = RateMatrix -> Acc (Matrix a) -> RateMatrix
type RecalibrationCondition a = Timestep -> Acc (Matrix a) -> Bool

-- | A Rated Operation takes these two as it's state for deciding what it needs
-- to do.
type RatedOperation = RateMatrix

-- | User Operation Type
-- Takes something of type `a` and applies a transformation 
type Operation a = Timestep -> Acc (Matrix a) -> Acc (Matrix a)

-- | A Pipeline is essentially a collection of values that need to be consistent
-- for fusion to be possible between different workloads.
type Pipeline a =
    (RegionDIMS, RateMatrix, MotionFun a, RecalibrationCondition a)
