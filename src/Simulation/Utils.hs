{-# LANGUAGE TypeOperators, FlexibleContexts #-}
module Simulation.Utils where

import Data.Array.Accelerate as A
import Data.Array.Accelerate.LLVM.Native as CPU
import Data.Array.Accelerate.Data.Complex
import Graphics.Gloss
import Graphics.Gloss.Data.ViewPort
import Graphics.Gloss.Accelerate.Data.Picture

import Data.Array.Accelerate.System.Random.MWC as Random
import Data.Random hiding ( uniform )
import qualified Data.Random.Distribution.Bernoulli as B

stepWrapper :: (m -> m)
            -> ViewPort
            -> Float
            -> m 
            -> m
stepWrapper f _ _ = f

stepWrapperT :: (Float -> m -> m)
             -> ViewPort
             -> Float
             -> m
             -> m
stepWrapperT f _ = f

bernoulli
    :: (Distribution (B.Bernoulli b) a, Shape sh, Elt a)
    => b
    -> sh :~> a
bernoulli ratio _sh gen = sampleFrom gen (B.bernoulli ratio)



blurStencil :: Stencil3x3 (Complex Float) -> Exp (Complex Float)
blurStencil
    ((q,w,e)
    ,(a,s,d)
    ,(z,x,c)) =
        res
    where
        summed = Prelude.sum $ [q, w, e, a, s, d, z, x, c]
        res = summed * (1/9)
