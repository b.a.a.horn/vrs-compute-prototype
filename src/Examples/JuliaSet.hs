{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE RebindableSyntax    #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ViewPatterns        #-}

-- This is directly from:
-- https://github.com/AccelerateHS/accelerate-examples
-- 
--
-- It's BSD3 licenced, to the Accelerate Team

module Examples.JuliaSet where

import Data.Array.Accelerate as A hiding ( fromInteger )
import Data.Array.Accelerate.Data.Complex
import Visualization.Julia
import Data.Array.Accelerate.LLVM.Native as CPU

import Prelude                                            ( fromInteger )
import qualified Prelude                                  as P

import VRC.Primitives
import VRC.Types

-- Our patched version of the accelerate julia set example
--
-- This is an easier version to work with our prexisting testing, but there is a
-- older version in the repo that directly patched the old code.
juliaPixel
    :: forall a. (Num a, RealFloat a, FromIntegral Int a)
    => Int                                      -- ^ image width
    -> Int                                      -- ^ image height
    -> (Exp Float -> Exp (Complex a) -> Exp (Complex a))
                                                -- ^ iteration function
    -> Exp Float                                -- ^ current time
    -> Exp a                                    -- ^ centre x
    -> Exp a                                    -- ^ centre y
    -> Exp a                                    -- ^ view width
    -> Exp Int32                                -- ^ iteration limit
    -> Exp a                                    -- ^ divergence radius
    -> Exp DIM2 
    -> Exp (Complex a, Int32)
juliaPixel
    screenX screenY 
    next
    t
    x0 y0
    width limit radius position
      = 
        zn
    where
        complexOfPixel :: Exp DIM2 -> Exp (Complex a)
        complexOfPixel (unlift -> Z :. y :. x) =
            let
              -- Adjusting the position we are looking at by adding the offset.
              y' = y
              x' = x
              height = P.fromIntegral screenY / P.fromIntegral screenX * width
              xmin   = x0 - width  / 2
              ymin   = y0 - height / 2
              --
              re     = xmin + (fromIntegral x' * width)  / fromIntegral (constant screenX)
              im     = ymin + (fromIntegral y' * height) / fromIntegral (constant screenY)
          in
          lift (re :+ im)

        -- Divergence condition
        --
        dot :: Exp (Complex a) -> Exp a
        dot (unlift -> x :+ y) = x*x + y*y

        -- Take a single step of the recurrence relation
        --
        step :: Exp (Complex a, Int32) -> Exp (Complex a, Int32)
        step (T2 z i) = T2 (next t z) (i + 1)

        z0 = complexOfPixel position
        zn = while (\zi -> snd zi < limit && dot (fst zi) < radius) step (T2 z0 0)



type CFI = (Complex Float, Int32)
juliaMotion :: MotionFun CFI
juliaMotion rm v =
        n
    where
        motionRegion :: Exp DIM2 -> Exp Int
        motionRegion (unlift -> Z:.y:.x) =
            let 
                k = constant 1 :: Exp Int
                summed = A.sum $ A.flatten $ A.map A.snd $ readRegion (32, 32) (32*x) (32*y) v
                res = the $ summed
            in
                k 
                -- fromIntegral $ res :: Exp Int

        -- Sum each region
        -- k =  sum $ A.map A.snd $ readRegion (32, 32) (32*X) (32*Y) x
        n = generate (shape rm) (\ix -> motionRegion ix)

        --res = backpermute (shape rm)
