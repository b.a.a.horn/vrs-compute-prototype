{-# LANGUAGE LambdaCase, BlockArguments  #-}
module Examples.GameOfLife where

import Data.Array.Accelerate as A

gameOfLifeStencil :: Stencil3x3 Bool -> Exp Bool
gameOfLifeStencil ((q,w,e), (r,a,t), (y,u,p)) =
    a & match \case
        True_  ->
            cond (isTwo A.|| isThree) True_ False_
        False_ ->
            cond isTwo True_ False_
    where
        summed = Prelude.sum $ Prelude.map boolToInt [q, w, e, r, t, y, u, p]

        isTwo = summed A.== 2
        isThree = summed A.== 3
