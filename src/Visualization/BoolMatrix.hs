module Visualization.BoolMatrix where

import Data.Array.Accelerate as A
import Data.Array.Accelerate.LLVM.Native as CPU
import Graphics.Gloss
import Graphics.Gloss.Accelerate.Data.Picture

-- | Convert a Matrix of bools to one that can be displayed.
renderBM :: Acc (Matrix Bool) -> Picture
renderBM x = bitmapOfArray (run $ A.map f x) True
    where
        f = \x -> cond x 0xff000000 0x00ffffff
