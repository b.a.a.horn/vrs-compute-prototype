module Visualization.RateMatrix where

import Data.Array.Accelerate as A
import Data.Array.Accelerate.LLVM.Native as CPU

import Graphics.Gloss
import Graphics.Gloss.Accelerate.Data.Picture

import VRC.Types

scaleColor :: Exp Int -> Exp Word32
scaleColor x = 0x7f00f000 - x''*0x0f00
    where
        x'' = A.fromIntegral $ unlift x :: Exp Word32
        x' = A.fromIntegral $ unlift (1 `div` (x+1)) :: Exp Word32

renderRM :: RegionDIMS -> RateMatrix -> Picture
renderRM (width, height) rm =
        scale width' height' $ bitmapOfArray (run $ A.map f rm) True
    where
        f = scaleColor
        width' = Prelude.fromIntegral width
        height' = Prelude.fromIntegral height
