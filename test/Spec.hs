import Test.HUnit

-- We'll be running our tests on the CPU
import Data.Array.Accelerate.LLVM.Native as CPU

readRegionTests :: Test
readRegionTests = TestCase (
    do
        assertEqual "for (foo 3)," (1,2) (1,2)
    )


writeRegionTests :: Test
writeRegionTests = TestCase (
    do
        assertEqual "for (foo 3)," (1,2) (1,2)
    )


tests = TestList [
        readRegionTests,
        writeRegionTests
    ]

main :: IO Counts
main = runTestTT tests
