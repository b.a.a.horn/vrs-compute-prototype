module RatedGOL where

import Simulation.Utils (stepWrapper, bernoulli)

import Data.Array.Accelerate as A
import Data.Array.Accelerate.LLVM.Native as CPU
import Data.Array.Accelerate.System.Random.MWC as Random

import Graphics.Gloss

import Debug.Trace
import VRC.Pipeline
import VRC.Types
import VRC.Utils (increasingMotion, nopRecalibration)
import Visualization.RateMatrix (renderRM)
import Visualization.BoolMatrix (renderBM)

import Examples.GameOfLife

gameOfLifeStep :: (Pipeline Bool, Timestep, Acc (Matrix Bool))
               -> (Pipeline Bool, Timestep, Acc (Matrix Bool))
gameOfLifeStep (pipeline@(dims, rm, _, _), ts, inp) =
        trace (show ts) (nPipeline, ts + 1, out')
    where 
        f = ratedStencil rm dims gameOfLifeStencil
        -- ts `mod` 8 is just making sure 
        ((rd, nRM, mf, rc), op, out) = ratedStep pipeline ts f inp
        nPipeline = (rd, use $ run nRM, mf, rc)
        -- Render it, else we go slow. Only needed for real time work.
        out' = use $ run out

gameOfLifeSetup :: RegionDIMS
                -> Acc (Matrix Bool)
                -> Pipeline Bool
gameOfLifeSetup dims =
        ratedSetup dims motionf conditionf
    where
        motionf = increasingMotion
        conditionf = nopRecalibration

renderRated :: (Pipeline Bool, Timestep, Acc (Matrix Bool))
            -> Picture
renderRated ((dims, rm, _, _), _, dat) =
    pictures [renderBM dat, renderRM dims rm]

entry :: (Int, Int) -> Float -> Int -> IO ()
entry (width, height) prob ups = do
    vs <- randomArray (bernoulli prob)
            (Z :. height :. width) :: IO (Matrix Bool)
    
    let initial = use vs
    let pipeline = gameOfLifeSetup (32, 32) initial

    simulate
        (InWindow "Uniform Game Of Life" (width*5, height*5) (0, 0))
        white
        ups
        (pipeline, 0, initial)
        renderRated
        (stepWrapper gameOfLifeStep)
