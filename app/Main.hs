module Main where

import System.Environment   
import Data.List

import qualified UniformGOL
import qualified RatedGOL

import qualified UniformJulia
import qualified RatedJulia


gol :: ((Int, Int) -> Float -> Int -> IO ())
    -> IO ()
gol f = f (128, 128) 0.005 10

julia :: ((Int, Int) -> Int -> IO ())
      -> IO ()
julia f = f (1024, 1024) 10

dispatch name =
    case name of
        "gol-rated" -> gol RatedGOL.entry
        "gol-uniform" -> gol UniformGOL.entry
        "julia-uniform" -> julia UniformJulia.entry
        "julia-rated" -> julia RatedJulia.entry
        _ -> putStrLn "unknown command"

main :: IO ()
main = do
    -- quick/easy dispatch to the different functions.
    -- we can do proper argument handling later.
    args <- getArgs

    if null args
       then putStrLn "say what you want ran"
       else
        dispatch (head args)
