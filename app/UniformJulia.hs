{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
module UniformJulia where

import Simulation.Utils (stepWrapperT, bernoulli, blurStencil)

import Data.Array.Accelerate as A
import Data.Array.Accelerate.LLVM.Native as CPU
import Data.Array.Accelerate.System.Random.MWC as Random
import Data.Array.Accelerate.Data.Complex

import Data.Int

import Graphics.Gloss
import Visualization.Julia (renderJulia)

import Examples.JuliaSet

import Debug.Trace

juliaSet :: Int -> Int -> Float -> Acc (Matrix (Complex Float, Int32))
juliaSet width height time =
        trace (show time) res
    where
        time' = constant time
        f = \t z -> z A.^ (2:: Exp Int) + mkPolar 0.7885 t

        js = juliaPixel width height f time' 0 0 4 255 16.0
        res = generate (constant (Z:.height:.width :: DIM2)) js :: Acc (Matrix (Complex Float, Int32))

juliaSetStep :: Int
             -> Int
             -> Float
             -> (Float, (Matrix (Complex Float, Int32)))
             -> (Float, (Matrix (Complex Float, Int32)))
juliaSetStep width height ts (prevTS, inp) =
        (newTS, run $ res)
    where
        newTS = prevTS + ts
        res = juliaSet width height (newTS*0.25)

entry :: (Int, Int) -> Int -> IO ()
entry (width, height) ups = do
    let vs = (0.0, run $ juliaSet width height 0.0)
    simulate
        (InWindow "Uniform Game Of Life" (width, height) (1, 1))
        white
        15
        vs
        renderJulia
        (stepWrapperT $ juliaSetStep width height)
