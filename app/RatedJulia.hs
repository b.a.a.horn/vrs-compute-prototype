{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
module RatedJulia where

import Simulation.Utils (stepWrapper, bernoulli)

import Data.Array.Accelerate as A
import Data.Array.Accelerate.LLVM.Native as CPU
import Data.Array.Accelerate.System.Random.MWC as Random
import Data.Array.Accelerate.Data.Complex

import Data.Int

import Graphics.Gloss
import Visualization.Julia (renderJulia)

import Examples.JuliaSet

import Debug.Trace

import VRC.Pipeline
import VRC.Types
import VRC.Utils (increasingMotion, nopRecalibration)
import Visualization.RateMatrix (renderRM)
import VRC.Utils (increasingMotion, nopRecalibration)


juliaSet :: Int -> Int -> Float -> Acc (Matrix CFI)
juliaSet width height time =
        trace (show time) res
    where
        time' = constant time
        f = \t z -> z A.^ (2:: Exp Int) + mkPolar 0.7885 t

        js = juliaPixel width height f time' 0 0 4 255 16.0
        idx = generate (constant (Z:.height:.width :: DIM2)) id
        res = A.map js idx :: Acc (Matrix (Complex Float, Int32))

juliaSetStep :: Int
             -> Int
             -> Float
             -> (Float, (Matrix CFI))
             -> (Float, (Matrix CFI))
juliaSetStep width height ts (prevTS, inp) =
        (newTS, run $ res)
    where
        newTS = prevTS + ts
        res = juliaSet width height (newTS*0.25)

jss :: Int -> Int
    -> (Pipeline CFI, Timestep, Acc (Matrix CFI))
    -> (Pipeline CFI, Timestep, Acc (Matrix CFI))
jss width height x@(pipeline@(dims, rm, _, _), ts, _) =
        x'
         
    where
        time' = constant 0.0
        jsf = \t z -> z A.^ (2:: Exp Int) + mkPolar 0.7885 t
        js = juliaPixel width height jsf time' 0 0 4 255 16.0
        jsWrap = \_ -> js

        idx = generate (constant (Z:.height:.width :: DIM2)) id
        
        -- BORKED HERE. NEED TO FIX TYPES
        --f = ratedMap rm dims jsWrap
       
        --((rd, nRM, mf, rc), op, out) = ratedStep pipeline ts f idx
        --nPipeline = (rd, use $ run nRM, mf, rc)
        x' = x

juliaSetup :: RegionDIMS
           -> Acc (Matrix CFI)
           -> Pipeline CFI
juliaSetup dims =
        ratedSetup dims motionf conditionf
    where 
        motionf = juliaMotion
        conditionf = nopRecalibration


renderRated :: (Pipeline CFI, Timestep, Acc (Matrix CFI))
            -> Picture
renderRated ((dims, rm, _, _), _, dat) =
    pictures [renderJulia (0.0, run $ dat), renderRM dims rm]


entry :: (Int, Int) -> Int -> IO ()
entry (width, height) ups = do
    let vs = juliaSet width height 0.0

    let pipeline = juliaSetup (32, 32) vs
    let initial = (pipeline, 0, vs)

    simulate
        (InWindow "Uniform Game Of Life" (width, height) (1, 1))
        white
        ups
        initial
        renderRated
        (stepWrapper $ jss width height)
