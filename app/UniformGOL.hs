module UniformGOL where

import Simulation.Utils (stepWrapper, bernoulli)

import Data.Array.Accelerate as A
import Data.Array.Accelerate.LLVM.Native as CPU
import Data.Array.Accelerate.System.Random.MWC as Random

import Graphics.Gloss
import Visualization.BoolMatrix (renderBM)

import Examples.GameOfLife

gameOfLifeStep :: Acc (Matrix Bool) -> Acc (Matrix Bool)
gameOfLifeStep inp =
        res
    where
        step = stencil gameOfLifeStencil wrap inp
        -- save the results to avoid slowing it down.
        res = use (run step)

entry :: (Int, Int) -> Float -> Int -> IO ()
entry (width, height) prob ups = do
    vs <- randomArray (bernoulli prob) (Z :. height :. width) :: IO (Matrix Bool)
    
    simulate
        (InWindow "Uniform Game Of Life" (width*5, height*5) (0, 0))
        white
        ups
        (use vs)
        renderBM
        (stepWrapper gameOfLifeStep)
