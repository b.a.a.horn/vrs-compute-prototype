# VRS-Based Compute Primitive


## Project Structure

We have four components:

### Examples

Our Example code, which was used to validate the work.

### Simulation

Utils for simulation, one is for getting a random distribution and the other is
a step helper to wrap a normal step function with the gloss viewport.

### Visualization

Helper functions for generating Gloss UIs showing off various data structures.

### VRC

The core of the project, this is where the variable rate logic is implemented.

* `Types.hs` - Type definitions
* `Pipeline.hs` - Pipeline based variable rate workloads. What you should ideally
  be using.
* `RegionOperators.hs` - Operations like Map/Stencil that work on regions.
* `Primitives.hs` - Ideally, this is what would be provided by the core
  accelerate language to make this easier to build.
* `Utils.hs` - Helper functions 

## Limitations

This is a prototype and doesn't really preform that well. We have to `run` in
one annoying place, but that should be fixable.

Our examples being simulations need us cache the results of the computation
every step. Real work could skip this and just get the final result.

Also, the code just runs on the CPU due to CUDA versions (I have 11.2 installed
due to running 460 series drivers, but Accelerate only supports CUDA 10)
